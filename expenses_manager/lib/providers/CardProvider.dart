import 'dart:collection';
import 'dart:convert';

import 'package:expenses_manager/models/CardModel.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CardProvider with ChangeNotifier {
  List<CardModel> cards = [];

  UnmodifiableListView<CardModel> get allCards => UnmodifiableListView(cards);

  void initialState() {
    syncDataWithProvider();
  }

  void addCard(CardModel card) {
    cards.add(card);
    updateSharedPreferences();
    notifyListeners();
  }

  void removeCard(CardModel _card) {
    cards.removeWhere((card) => card.number == _card.number);

    updateSharedPreferences();
    notifyListeners();
  }

  int getCardsLength() {
    return cards.length;
  }

  Future updateSharedPreferences() async {
    List<String> myCards = cards.map((e) => json.encode(e.toJson())).toList();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setStringList('cards', myCards);
  }

  Future syncDataWithProvider() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var reslut = prefs.getStringList('cards');

    if (reslut != null) {
      cards = reslut.map((e) => CardModel.fromJson(json.decode(e))).toList();
    }

    notifyListeners();
  }
}
