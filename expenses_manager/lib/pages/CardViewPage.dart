import 'package:expenses_manager/components/CardView.dart';
import 'package:expenses_manager/main.dart';
import 'package:expenses_manager/models/CardModel.dart';
import 'package:expenses_manager/providers/CardProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CardViewPage extends StatefulWidget {
  final CardModel card;

  const CardViewPage({Key key, @required this.card}) : super(key: key);
  @override
  _CardViewPageState createState() => _CardViewPageState();
}

class _CardViewPageState extends State<CardViewPage> {
  void onRemove(card) {
    Provider.of<CardProvider>(context, listen: false).removeCard(card);

    Navigator.of(context).pop(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(238, 241, 242, 1),
      appBar: AppBar(
        centerTitle: true,
        brightness: Brightness.light,
        title: Text(
          'Card Page',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Color.fromRGBO(238, 241, 242, 1),
        elevation: 0,
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
            ),
            color: Colors.black45,
            onPressed: () {
              Navigator.canPop(context)
                  ? Navigator.of(context).pop(true)
                  : Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomePage()));
            }),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.delete, color: Colors.black45),
              onPressed: () {
                onRemove(widget.card);
              })
        ],
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.all(20),
        child: Transform.translate(
            offset: Offset.fromDirection(0, 15),
            child: Container(
              height: 210,
              child: CardView(card: widget.card),
            )),
      )),
    );
  }
}
