import 'package:expenses_manager/main.dart';
import 'package:expenses_manager/models/CardModel.dart';
import 'package:expenses_manager/providers/CardProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddCardPage extends StatefulWidget {
  @override
  _AddCardPageState createState() => _AddCardPageState();
}

class _AddCardPageState extends State<AddCardPage> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController numberController = new TextEditingController();
  TextEditingController bankNameController = new TextEditingController();
  TextEditingController availableController = new TextEditingController();
  TextEditingController currencyController = new TextEditingController();

  void onAdd() {
    CardModel newCard = CardModel(
        name: nameController.text,
        number: numberController.text,
        bankName: bankNameController.text,
        available: num.tryParse(availableController.text),
        currency: currencyController.text);
    Provider.of<CardProvider>(context, listen: false).addCard(newCard);

    Navigator.of(context).pop(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(238, 241, 242, 1),
        appBar: AppBar(
          centerTitle: true,
          brightness: Brightness.light,
          title: Text(
            'Add Card',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          backgroundColor: Color.fromRGBO(238, 241, 242, 1),
          elevation: 0,
          leading: IconButton(
              icon: Icon(
                Icons.chevron_left,
              ),
              color: Colors.black45,
              onPressed: () {
                Navigator.canPop(context)
                    ? Navigator.of(context).pop(true)
                    : Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
              }),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        controller: nameController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 3.0),
                            border: InputBorder.none,
                            hintText: "Card Name",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        controller: numberController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 3.0),
                            border: InputBorder.none,
                            hintText: "Card Number",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        controller: bankNameController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 3.0),
                            border: InputBorder.none,
                            hintText: "Bank Name",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        controller: availableController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 3.0),
                            border: InputBorder.none,
                            hintText: "Available Balance",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Container(
                      padding: EdgeInsets.all(5.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: TextField(
                        controller: currencyController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 3.0),
                            border: InputBorder.none,
                            hintText: "Currency",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    SizedBox(height: 15.0),
                    MaterialButton(
                        elevation: 0,
                        minWidth: double.infinity,
                        padding: EdgeInsets.all(15),
                        color: Colors.blue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Text('Add',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold)),
                        onPressed: () => onAdd())
                  ],
                ),
              )),
        ));
  }
}
