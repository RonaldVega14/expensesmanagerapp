import 'package:expenses_manager/models/TransactionModel.dart';

class CardModel {
  final int id;
  final String name;
  final String bankName;
  final String number;
  final String currency;

  final int available;

  final List<TransactionModel> transactions;

  CardModel(
      {this.id,
      this.name,
      this.bankName,
      this.number,
      this.currency,
      this.available,
      this.transactions});
//From CardModel to Json
  Map toJson() => {
        'id': id,
        'name': name,
        'bankName': bankName,
        'number': number,
        'currency': currency,
        'available': available,
        'transactions': transactions
      };
//From Json to cardModel
  CardModel.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        bankName = json['bankName'],
        number = json['number'],
        currency = json['currency'],
        available = json['available'],
        transactions = json['transactions'];
}
