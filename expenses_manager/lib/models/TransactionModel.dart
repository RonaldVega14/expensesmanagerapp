class TransactionModel {
  final int id;
  final String name;
  final int price;
  final String type;
  final String currency;

  TransactionModel({this.id, this.name, this.price, this.type, this.currency});

  Map toJson() => {
        'id': id,
        'name': name,
        'price': price,
        'type': type,
        'currency': currency,
      };

  TransactionModel.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        price = json['price'],
        type = json['type'],
        currency = json['currency'];
}
