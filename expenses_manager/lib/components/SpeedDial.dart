import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class SpeedDialWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      // both default to 16
      marginRight: 14,
      marginBottom: 14,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 20.0),
      // this is ignored if animatedIcon is non null
      // child: Icon(Icons.add),
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      onOpen: () => {},
      onClose: () => {},
      tooltip: 'Open Menu',
      backgroundColor: Colors.white,
      foregroundColor: Colors.black,
      elevation: 4.0,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
            child: Icon(Icons.credit_card),
            backgroundColor: Colors.grey,
            label: 'Add Card',
            labelStyle: TextStyle(fontSize: 16.0),
            onTap: () => print('Add Card')),
        SpeedDialChild(
          child: Icon(Icons.attach_money),
          backgroundColor: Colors.blueGrey,
          label: 'Add Transaction',
          labelStyle: TextStyle(fontSize: 16.0),
          onTap: () => print('Transaction'),
        ),
        SpeedDialChild(
          child: Icon(Icons.person),
          backgroundColor: Colors.black87,
          label: 'Profile',
          labelStyle: TextStyle(fontSize: 16.0),
          onTap: () => print('Profile'),
        ),
      ],
    );
  }
}
