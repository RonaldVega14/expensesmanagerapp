import 'package:expenses_manager/components/CardView.dart';
import 'package:expenses_manager/models/CardModel.dart';
import 'package:expenses_manager/pages/CardViewPage.dart';
import 'package:flutter/material.dart';

class CardList extends StatelessWidget {
  final List<CardModel> cards;

  const CardList({Key key, @required this.cards}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: getChildrenCards(context),
    );
  }

  List<Widget> getChildrenCards(BuildContext context) {
    return cards
        .map((card) => GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CardViewPage(card: card),
                    ));
              },
              child: CardView(card: card),
            ))
        .toList();
  }
}
