import 'package:expenses_manager/components/CardList.dart';
import 'package:expenses_manager/components/CardView.dart';
import 'package:expenses_manager/components/SpeedDial.dart';
import 'package:expenses_manager/components/TransactionView.dart';
import 'package:expenses_manager/models/CardModel.dart';
import 'package:expenses_manager/models/TransactionModel.dart';
import 'package:expenses_manager/pages/AddCardPage.dart';
import 'package:expenses_manager/providers/CardProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(ChangeNotifierProvider<CardProvider>(
    create: (context) => CardProvider(),
    child: new MaterialApp(
      theme: ThemeData(),
      home: HomePage(),
    )));

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Provider.of<CardProvider>(context).initialState();
    return Scaffold(
        floatingActionButton: SpeedDialWidget(),
        backgroundColor: Color.fromRGBO(238, 241, 242, 1),
        appBar: AppBar(
          centerTitle: true,
          brightness: Brightness.light,
          title: Text(
            'Home Page',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          backgroundColor: Color.fromRGBO(238, 241, 242, 1),
          elevation: 0,
          leading: null,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.add),
                color: Colors.black45,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddCardPage()));
                })
          ],
        ),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: ListView(
              children: <Widget>[
                (Provider.of<CardProvider>(context).getCardsLength() > 0
                    ? Container(
                        height: 210,
                        child: Consumer<CardProvider>(
                          builder: (context, cards, child) =>
                              CardList(cards: cards.allCards),
                        ),
                      )
                    : Container(
                        height: 210,
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(20.0)),
                        child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              'To Add your new card click the \n + \n button in the top right corner.',
                              textAlign: TextAlign.center,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            )),
                      )),
                SizedBox(height: 30),
                Text(
                  'Last Transactions',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.black45),
                ),
                SizedBox(height: 15.0),
                TransactionView(
                    transaction: TransactionModel(
                        name: 'Shopping',
                        price: 1000,
                        type: '-',
                        currency: 'USD')),
                TransactionView(
                    transaction: TransactionModel(
                        name: 'Food', price: 200, type: '-', currency: 'USD')),
                TransactionView(
                    transaction: TransactionModel(
                        name: 'Business',
                        price: 1000,
                        type: '+',
                        currency: 'USD')),
                TransactionView(
                    transaction: TransactionModel(
                        name: 'Development',
                        price: 400,
                        type: '+',
                        currency: 'USD')),
                TransactionView(
                    transaction: TransactionModel(
                        name: 'Development',
                        price: 325,
                        type: '+',
                        currency: 'USD'))
              ],
            ),
          ),
        ));
  }
}
