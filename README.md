**Expenses Manager**
A Flutter application built from scratch using Provider for state management.

Features:

- Add Multiple Cards.

- Add Transactions. 

- Profile Image picker. (Gallery or camera)


**App Screenshots**

<img src="images/noCards.png" height = "400">

<img src="images/home.png" height = "400">

<img src="images/menuHome.png" height = "400">
